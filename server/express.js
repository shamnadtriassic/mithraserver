'use strict';
const express = require('express');
const app = express();
const LexMethods = require("../lex/lexpost");

class Express {
  constructor () {
      console.log("Inside constructor");
  }

  createServer () {
    var server = app.listen(8082, "127.0.0.1", function () {
       var host = server.address().address
       var port = server.address().port

       console.log("Example app listening at http://%s:%s/", host, port)
    })

    app.get('/', function (req, res) {
       res.send('Hello World Express');
    });

    app.get('/testIntent/:message', function (req, res) {
       const message = req.params.message;
       const lexmethods = new LexMethods();
       const response = lexmethods.postText(message, res);
    });
  }
}

module.exports = Express;
